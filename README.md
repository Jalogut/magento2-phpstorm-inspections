# Magento PHPStorm Inspections

Set of static rules for the different Magento versions used to setup global PHPStorm configuration compatible with all versions

## Setup

### Clone repo

```
git clone git@bitbucket.org:Jalogut/magento2-phpstorm-inspections.git
```
	
### Init rules for your magento version

```
cd magento2-phpstorm-inspections
./setup-rules.sh <magento2_project_absolute_path> <magento_version>
```
	
**NOTE:** `<magento_version>` valid values are `2-1` or `2-2`
	
Example
	
```
./setup-rules.sh /var/www/magento2 2-2
```
	
### Configure PHPStorm

0. Setup `phpcs` binary
	
	* `PHPStorm > Preferences > Languages & Frameworks > PHP > Code Sniffer`
		* Select path `magento2-phpstorm-inspections/bin/phpcs` 

0. Setup `phpmd` binary
	
	* `PHPStorm > Preferences > Languages & Frameworks > PHP > Mess Detector`
		* Select path `magento2-phpstorm-inspections/bin/phpmd`	

0. Setup inspections

	* `PHPStorm > Preferences > Editor > Inspections > PHP > PHP Code Sniffer Validation`
		* `magento2-phpstorm-inspections/rules/phpcs/ruleset.xml`

	* `PHPStorm > Preferences > Editor > Inspections > PHP > Mess Detector validation`
		* `magento2-phpstorm-inspections/rules/phpmd/ruleset.xml`

If you have any issues, please see the [official PHPStorm documentation](https://confluence.jetbrains.com/display/PhpStorm/PHP+Code+Quality+Tools)

## Switch between magento versions

If you happen to work on different magento versions, you can quickly switch the rules using the following command

```
cd magento2-phpstorm-inspections
./setup-rules.sh <magento2_project_absolute_path> <magento_version>
```