#!/bin/bash

# $1 must be the absolute path to the magento project
rm -f config/magento2-project && ln -sf $1 config/magento2-project

# $2 version of magento rules [2-1 | 2-2]
rm -rf rules/* && cp -r config/magento-$2/* rules/
